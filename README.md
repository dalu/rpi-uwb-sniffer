RPI UWB Sniffer
===============

This program enable sniffing ultra wide band (UWB) packet by 
forwarding them to a given ethernet address.

From there you just need to use wireshark (or tcpdump) and filter on the 
MAC address and ethernet prototype (9999 by default)


Usage
=====

~~~
Usage: uwb-sniffer [OPTIONS]* <dst_macaddr>
  -P, --prototype=INT        ethernet prototype (default: 9999)
  -i, --interface=STRING     ethernet interface (default: "eth0")
  -c, --channel=INT          channel
  -b, --bitrate=INT          bitrate (in kbps)
  -p, --prf=INT              pulse rate frequency (in MHz)
      --tx_plen=INT          preamble length
      --rx_pac=INT           preamble accumulation
      --tx_pcode=INT         TX preamble code
      --rx_pcode=INT         RX preamble code
      --tx_delay=FLOAT       antenna TX delay (in meters)
      --rx_delay=FLOAT       antenna RX delay (in meters)
  -v, --verbose              verbose mode
  -V, --version              show version information
~~~


Example:
~~~sh
uwb-sniffer -i eth1 -P 6666                                 \
    -c 5 -b 6800 -p 64                                      \
    --tx_pcode 10 --rx_pcode 10 --tx_plen 128 --rx_pac 8
~~~

If using tcpdump on the remote side:
~~~sh
tcpdump ether proto 6666 and ether src aa:bb:cc:dd:ee:ff
~~~


Wiring
======

On a raspberry pi connect the decawave module to the following pin.

|RPI | DWM1000 | Meaning
|----|---------|-------------------
| 14 | GND     | Ground
| 16 | IRQ     | DWM1000 Interrupt
| 17 | 3V3     | Power 3.3V
| 18 | RESET   | DWM1000 Reset
| 19 | MOSI    | SPI MOSI
| 21 | MISO    | SPI MISO
| 22 | WAKEUP  | DWM1000 Wakeup
| 23 | SCLK    | SPI Clock
| 24 | CS      | SPI Chip Select


Building
========
~~~sh
git clone --recurse-submodules https://gitlab.inria.fr/dalu/rpi-uwb-sniffer/
cd rpi-uwb-sniffer
sh build.sh
~~~


TODO
====
* use dw1000 double buffer to improve performance
