#!/bin/sh

decawave_drivers=decawave-drivers
bitters=bitters

VERSION="unknown"

if which git > /dev/null; then
    git_count=`git rev-list HEAD --count`
    git_short=`git rev-parse --short HEAD`
    git_abbrev=`git rev-parse --abbrev-ref HEAD`
    git_tag=`git describe --exact-match --abbrev=0 --tags 2> /dev/null`

    if [ -n "${git_tag}" ]; then
	VERSION=${git_tag}
    else
	VERSION="${git_abbrev}-${git_count} (${git_short})"
    fi
fi


gcc src/*.c 						\
    -D_GNU_SOURCE					\
    -I include 						\
    ${decawave_drivers}/port/unix/dw/osal/src/*.c 	\
    -I ${decawave_drivers}/port/unix/dw/osal/include	\
    ${decawave_drivers}/hw/drivers/dw1000/src/*.c 	\
    -DDW1000_WITH_PROPRIETARY_PREAMBLE_LENGTH=1		\
    -DDW1000_WITH_PROPRIETARY_SFD=1			\
    -DDW1000_WITH_PROPRIETARY_LONG_FRAME=0		\
    -DDW1000_WITH_SFD_TIMEOUT=0				\
    -I ${decawave_drivers}/hw/drivers/dw1000/include 	\
    ${bitters}/src/*.c					\
    -I ${bitters}/include				\
    -DBITTERS_SPI_WITH_ASSERT    			\
    -DBITTERS_GPIO_WITH_ASSERT				\
    -DBITTERS_WITH_GPIO_IRQ				\
    -DBITTERS_WITH_THREADS				\
    -DBITTERS_SILENCE_RPI_WARNING			\
    -DVERSION="\"${VERSION}\""				\
    -lpopt -lm -pthread					\
    -o uwb-sniffer
